﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using ACS.Common.Collections;
using ACS.Common.Logging;
using ACS.Common.Serialization;
using ACS.Common.Xml;

namespace Presentation.ControlBuilder
{
    
    [XmlRoot]
    [Serializable]        
    public class XmlObject : IXmlSerializable
    {
        public XmlObject()
        {
            Objects = new List<XmlObject>();
            Attributes = new Parameters();
        }

        public XmlObject(Parameters inParameters)
        {
            Objects = new List<XmlObject>();
            Attributes = inParameters;
        }

        #region public properties

        public XmlObject Object { get; set; }
      
        [XmlAttribute]
        public string Type { get ; set; }

        [XmlAttribute]
        public string Assembly { get; set; }
        
        [XmlAttribute]
        public string xmlns { get; set; }
        [XmlAttribute]
        public string Namespace { get; set; }
        
        [XmlElement]
        public Parameters Attributes { get; set; }

        [XmlElement]
        public virtual List<XmlObject> Objects { get; set; }

        #endregion public properties

        #region public methods
        public XmlObject GetFirtsChild()
        {   
            return Objects.Count >= 1 ? Objects[0] : null;            
        }

        #endregion public methods

        #region public methods
        public List<XmlObject> GetAllChildren()
        {
            return Objects.Count >= 1 ? Objects : null;
        }

        #endregion public methods

        #region IXmlSerizable methods
        public virtual void WriteXml(XmlWriter xmlWriter)
        {
            try
            {
                if (! String.IsNullOrEmpty(Type) )
                    xmlWriter.WriteStartElement(Type); 

                foreach( string attrKey in Attributes.Keys)
                    xmlWriter.WriteAttributeString(attrKey, Attributes[attrKey]);

                foreach (XmlObject child in Objects)
                {
                    child.WriteXml(xmlWriter);
                }

                if (!String.IsNullOrEmpty(Type))
                    xmlWriter.WriteEndElement();
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw exp;
            }
        }

        public virtual void ReadXml(XmlReader xmlReader)
        {
            try
            {

                    string sReader = xmlReader.ReadOuterXml();
                    XmlModel x = new XmlModel(XElement.Parse(sReader));                    

                    foreach (XAttribute attribute in x.Attributes())
                    {
                        switch (attribute.Name.LocalName)
                        {
                            case "Namespace":
                                Namespace = attribute.Value;
                                break;
                            case "Type":
                                Type = attribute.Value;
                                break;
                            case "Assembly":
                                Assembly = attribute.Value;
                                break;

                            //for transfare throgh WCF or WEB service
                            case "xmlns":
                                xmlns = attribute.Value;
                                break;
                            default:
                                Attributes.Add(attribute.Name.LocalName, attribute.Value);
                                break;
                        }
                    }

                    foreach (XNode n in x.Nodes())
                    {
                        if (n is XElement)
                        {
                            XmlModel xmlModel = new XmlModel(n as XElement);

                            string s = xmlModel.ToString();

                            if (xmlModel.Type != "XmlObject")
                            {
                                s = s.Replace(String.Format("<{0} ", xmlModel.Type), String.Format("<XmlObject Type=\"{0}\" ", xmlModel.Type));
                                s = s.Replace(String.Format("<{0}>", xmlModel.Type), String.Format("<XmlObject Type=\"{0}\">", xmlModel.Type));
                                s = s.Replace(String.Format("</{0}>", xmlModel.Type), "</XmlObject>");
                                //remove SOAP namespace
                                //this line need for transfare throgh WCF or WEB service                        
                                s = s.Replace(String.Format("xmlns=\"{0}\"", xmlns), String.Empty);
                            }

                            XmlObject obj = (XmlObject)ObjectSerialization.XmlReaderToObject(typeof(XmlObject), XElement.Parse(s).CreateReader());
                            Objects.Add(obj);
                        }

                        if (n is XComment)
                        {
                            //stub for comment
                        }
                    
                    }
               
                
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw exp;
            }
        }

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        #endregion

        #region Object methods

        public override string ToString()
        {
            return Namespace + Type;
        }

        #endregion Object methods
    }
}
