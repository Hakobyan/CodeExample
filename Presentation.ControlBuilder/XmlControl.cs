﻿using System;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using ACS.Common.IO;
using ACS.Common.Logging;
using ACS.Common.Xml;
using System.Text.RegularExpressions;
using ACS.Common.Serialization;


namespace Presentation.ControlBuilder
{
    /// <summary>
    /// description class Control, WebControl from xml file       
    /// </summary>
    [Serializable]
    [DataContract]
    [XmlRoot("Control")]
    public class XmlControl : XmlObject
    {
        public XmlControl()
        {

        }

        #region IXmlSerizable methods
        public override void WriteXml(XmlWriter xmlWriter)
        {
            try
            {
            if (!String.IsNullOrEmpty(Type))
            {
                xmlWriter.WriteStartElement("Control");
                xmlWriter.WriteAttributeString("Type", Type);
            }

            foreach (string attrKey in Attributes.Keys)
                xmlWriter.WriteAttributeString(attrKey, Attributes[attrKey]);

            foreach (XmlObject child in Objects)
            {
                child.WriteXml(xmlWriter);
            }

            if (!String.IsNullOrEmpty(Type))
                xmlWriter.WriteEndElement();
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }
        }

        public override void ReadXml(XmlReader xmlReader)
        {
            try
            {

                string sReader = xmlReader.ReadOuterXml();
                XmlModel x = new XmlModel(XElement.Parse(sReader));

                foreach (XAttribute attribute in x.Attributes())
                {
                    switch (attribute.Name.LocalName)
                    {
                        case "Namespace":
                            Namespace = attribute.Value;
                            break;
                        case "Type":
                            Type = attribute.Value;
                            break;
                        case "Assembly":
                            Assembly = attribute.Value;
                            break;

                        //for transfare throgh WCF or WEB service
                        case "xmlns":
                            xmlns = attribute.Value;
                            break;

                        default:
                            Attributes.Add(attribute.Name.LocalName, attribute.Value);
                            break;
                    }
                }

                foreach (XNode n in x.Nodes())
                {
                    if (n is XElement)
                    {
                        XmlModel xmlModel = new XmlModel(n as XElement);

                        string s = xmlModel.ToString();

                        //remove SOAP namespace
                        //this line need for transfare throgh WCF or WEB service                        
                        //s = s.Replace(String.Format("xmlns=\"{0}\"", xmlns), String.Empty);
                        s = Regex.Replace(s, "xmlns=\".*\"", string.Empty);

                        //////////////////////////////////////////////////////////////////////////////////////////
                        /// TTC , Hardcode request from Vardan Harutyunyan
                        //to change only <Control > tag to <Parameters> tag
                        switch (xmlModel.Type)
                        {
                            case "Parameters":
                            case "Parameter":
                                //...//
                                //Can be extended in case of any  whim of Vardan's
                                {
                                    s = s.Replace(String.Format("<{0} ", xmlModel.Type), String.Format("<Control Type=\"{0}\" ", xmlModel.Type));
                                    s = s.Replace(String.Format("<{0}>", xmlModel.Type), String.Format("<Control Type=\"{0}\">", xmlModel.Type));
                                    s = s.Replace(String.Format("</{0}>", xmlModel.Type), "</Control>");
                                }
                                break;
                        }                       
                        //////////////////////////////////////////////////////////////////////////////////////////
                        XmlControl obj = (XmlControl)ObjectSerialization.XmlReaderToObject(typeof(XmlControl), XElement.Parse(s).CreateReader());
                        Objects.Add(obj);
                    }

                    if (n is XComment)
                    {
                        //stub for comment
                    }

                }


            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw exp;
            }
        }

        #endregion IXmlSerizable methods
    }
}