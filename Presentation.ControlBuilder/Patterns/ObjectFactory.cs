﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using ACS.Common.Logging;

namespace Presentation.ControlBuilder
{
    /// <summary>
    /// Object Factory pattern
    /// Controls/forms generated based of xml (XmlObject)
    /// </summary>
    public class ObjectFactory
    {
        /// <summary>
        /// Get Control hashtable from collection of XMLControls
        /// </summary>        
        /// <param name="XMLControlsCollection">Collection of XMLControls</param>        
        /// <returns>hash table of Controls</returns>
        public static Hashtable GetObjectTypes(List<XmlObject> xmlObjectList)
        {
            if (xmlObjectList == null)
            {
                Exception exp = new ArgumentNullException("xmlObjectList is Null");
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);                
                throw exp;
            }

            Hashtable objectTypes = new Hashtable();

            for (int j = 0; j < xmlObjectList.Count; j++)
            {
                XmlObject xmlObject = xmlObjectList[j]; 
                Object ctrl = null;

                try
                {
                    if (!String.IsNullOrEmpty( xmlObject.Assembly ) )
                    {                   
                        Assembly assembly = Assembly.Load(xmlObject.Assembly);
                        Type type = assembly.GetType(String.Format("{0}.{1}", xmlObject.Namespace , xmlObject.Type) );
                        if (type == null)
                        {
                            LogWrapper.LogWarn(String.Format("No such '{0}' type in list of types, Assembly: {1}, Namspace: {2}", xmlObject.Type, xmlObject.Assembly, xmlObject.Namespace), MethodBase.GetCurrentMethod());
                            continue;
                        }

                        ctrl = Activator.CreateInstance(type);
                    }


                    objectTypes.Add(xmlObject.Type, ctrl);
                }
                catch(Exception exp) 
                {
                    LogWrapper.LogError(String.Format("No such '{0}' Type in list of types, Assembly: {1}, Namspace: {2}", xmlObject.Type, xmlObject.Assembly, xmlObject.Namespace), MethodBase.GetCurrentMethod());
                    LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);                    
                    continue;
                }

            }

            return objectTypes;
        }

        /// <summary>
        /// Create object/control or other presentesion element based on xml (xml object)
        /// </summary>
        /// <param name="controls"> list of described controls </param>
        /// <param name="xmlObject"> xml entity/object</param>
        /// <param name="adapter">particular adapter for creation presentation control</param>
        /// <returns></returns>
        public static Object Create(Hashtable controls, XmlObject xmlObject, IObjectAdapter adapter)
        {
            if (controls == null || xmlObject == null)
            {
                Exception exp = new ArgumentNullException("controls or xmlObject is NULL");
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);                
                throw exp;
            }

            try
            {                
                Type type = controls[xmlObject.Type].GetType();

                Object obj = Activator.CreateInstance(type);

                obj.Populate(controls, xmlObject, adapter);                

                return obj;
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(String.Format("No such '{0}' type in assembly {1}", xmlObject.Type, xmlObject.Assembly), MethodBase.GetCurrentMethod());                
                throw exp;
            }
                     
        }
    }
}
