﻿using System;
using System.Collections;
using System.Reflection;
using ACS.Common;
using ACS.Common.Logging;

namespace Presentation.ControlBuilder
{
    /// <summary>
    /// Extension class for helping object building
    /// </summary>
    public static class ObjectBuilderExtension 
    {   

        /// <summary>
        /// converting WebControl from XMLControl
        /// </summary>
        /// <param name="XMLCtrl">XMLControl for which must be converted</param>
        /// <param name="WebCtrl">result Web control</param>
        /// <returns>success or not</returns>
        public static void Populate(this Object objectInstance, Hashtable controls, XmlObject xmlObject, IObjectAdapter adapter)
        {
            if (xmlObject == null || objectInstance == null 
                || controls == null || adapter == null)
            {
                Exception exp = new ArgumentNullException("objectInstance, controls xmlObject or object adapter is NULL");
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw exp;
            }

            IDictionaryEnumerator enumeratorAttributes = xmlObject.Attributes.GetEnumerator();

            while (enumeratorAttributes.MoveNext())
            {
                PropertyInfo prInfo = objectInstance.GetType().GetProperty(enumeratorAttributes.Key.ToString());
                 
                //is there such property
                if (prInfo == null)
                {
                    EventInfo eventInfo = objectInstance.GetType().GetEvent(enumeratorAttributes.Key.ToString());

                    if (eventInfo != null)
                    {
                        Type t = objectInstance.GetType();

                        MethodInfo methodInfo = t.GetMethod(enumeratorAttributes.Value.ToString(), BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);

                        if (methodInfo != null)
                        {

                            Delegate handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, null, methodInfo, false);
                            if (handler != null)
                            {
                                eventInfo.AddEventHandler(objectInstance, handler);
                            }
                        }
                    }
                    continue;
                    
                }
                
                try
                {
                    CommonExtension.SetPropertyValue(enumeratorAttributes.Value, prInfo, objectInstance);                    
                }
                catch (Exception exp)
                {
                    LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                    continue;
                }
            }
            
            foreach (XmlObject xmlObjChild in xmlObject.Objects)
            {
                try
                {
                    Object childObj = ObjectFactory.Create(controls, xmlObjChild, adapter);
                    adapter.Adapt(objectInstance, childObj);
                }
                catch (Exception exp)
                {
                    LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                    continue;
                }
            }

        }
    }
}
