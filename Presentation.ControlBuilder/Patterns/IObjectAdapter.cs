﻿using System;

namespace Presentation.ControlBuilder
{
    /// <summary>
    /// presentation adapter pattern 
    /// </summary>
    public interface IObjectAdapter
    {   
        void Adapt( Object parentObj, Object childObj);
    }

   

}
