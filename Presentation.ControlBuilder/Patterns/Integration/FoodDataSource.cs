﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;

namespace eStuff.Samples
{   
    public class FoodDataSource : ObjectDataSource
    {

        // Public property for the user to choose their food type
        public string FoodType { get; set; }

        public FoodDataSource()
            : base(typeof(FoodData).FullName, "GetFood")
        {
            // Constructor parameters:
            // - ObjectDataSource complains if we don't have a TypeName set.
            // - Our custom object's SelectMethod is always called GetFood.

            // Hook up the ObjectCreating event so we can use our custom object
            ObjectCreating += new ObjectDataSourceObjectEventHandler(FoodDataSource_ObjectCreating); 
        }

        void FoodDataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = new FoodData(this);             
        }

        // Custom data object that returns yummy foods
        private sealed class FoodData
        {
            private FoodDataSource _dataSource;

            public FoodData(FoodDataSource dataSource)
            {
                _dataSource = dataSource;
            }

            public IEnumerable GetFood()
            {
                string foodType = _dataSource.FoodType;
                yield return foodType + " yummy 1";
                yield return foodType + " yummy 2";
                yield return foodType + " yummy 3";
                yield return foodType + " yummy 4";
            }

            public DataTable Select()
            {
                string foodType = _dataSource.FoodType;
                //List<string> l = new List<string>();                
                //l.Add( foodType + " yummy 1" );
                //l.Add( foodType + " yummy 2" );
                //l.Add( foodType + " yummy 3" );
                //l.Add( foodType + " yummy 4" );

                DataTable dt = new DataTable();
                dt.Columns.Add("Type");
                dt.Columns.Add("Name");

                dt.Rows.Add(new object[] { foodType, "yummy 1" });
                dt.Rows.Add(new object[] { foodType, "yummy 2" });


                return dt;
            }
        }
    }
}
