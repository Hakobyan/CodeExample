﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;

namespace ACS.Presentation.ControlBuilder.Patterns.Integration
{
    public class CustomDataSource : ObjectDataSource
    {
        public DataTable SelectCustomSource()
        {
            return new DataTable();
        }
    }
}
