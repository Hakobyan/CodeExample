﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;
using System.Data;

namespace eStuff.Samples
{   
    public class AS400DataSource : ObjectDataSource
    {   
        public string ProgramName {get; set;}

        public AS400DataSource()
            //: base(typeof(AS400Data).FullName, "Select")
        {
            // Constructor parameters:
            // - ObjectDataSource complains if we don't have a TypeName set.
            // - Our custom object's SelectMethod is always called GetFood.

            // Hook up the ObjectCreating event so we can use our custom object
            ObjectCreating += new ObjectDataSourceObjectEventHandler(AS400DataSource_ObjectCreating);
            //base.SelectMethod = "Select";
        }

        void AS400DataSource_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            e.ObjectInstance = new AS400Data(this);             
        }

      

       
    }

    // Custom data object that returns yummy foods
    public class AS400Data
    {
        private AS400DataSource _dataSource;

        public AS400Data(AS400DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public DataTable Select(string user, string password, string logon)
        {
            string pm = _dataSource.ProgramName;
            string foodType = "AS400";

            DataTable dt = new DataTable();
            dt.Columns.Add("Type");
            dt.Columns.Add("Name");

            dt.Rows.Add(new object[] { foodType, "yummy 1" });
            dt.Rows.Add(new object[] { foodType, "yummy 2" });

            return dt;
        }
    }
}
