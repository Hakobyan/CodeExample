﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Presentation.ControlBuilder
{
    /// <summary>
    /// Summary description for ControlTypes
    /// </summary>
    [XmlRoot]
    public class ControlTypes
    {
        public ControlTypes()
        {
            Controls = new List<XmlObject>();
        }
        [XmlElement("Control")]
        public List<XmlObject> Controls { get; set; }
    }
}
