﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Presentation.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace Presentation.Controllers
{
    public static class EventControllerExtention
    {
        /// <summary>
        /// Example call of presentation controller 
        /// </summary>
        /// <param name="xmlDelegat"></param>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void On_Button_Click(this ControlEventHandler xmlDelegat, object sender, System.EventArgs e)
        {
            Label lbl = ((sender as Control).Parent).FindChild("Label000") as Label;
            lbl.Text = DateTime.Now.ToLongTimeString();
            if ( xmlDelegat.Parameters.ContainsKey("ParameterFromEvent") )
                lbl.Text += " | " + xmlDelegat.Parameters["ParameterFromEvent"];
        }
    }
}
