﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Web;
using ACS.Common.IO;
using Presentation.ControlBuilder;
using ACS.Common.Serialization;

namespace WebTest
{
    public class Global : System.Web.HttpApplication
    {
        public static string PresentationExample1 = string.Empty;
        public static string PresentationExample2 = string.Empty;
        public static string PresentationExample3 = string.Empty;
        public static string PresentationExample4 = string.Empty;
        
        public static Hashtable TypesContainer = null;

        protected void Application_Start(object sender, EventArgs e)
        {
            // example of screens based on xml 
            StreamReader sr1 = File.OpenText(Path.Combine(HttpRuntime.AppDomainAppPath, @"App_Data\Examples\Example1_StandartControls.xml"));
            StreamReader sr2 = File.OpenText(Path.Combine(HttpRuntime.AppDomainAppPath, @"App_Data\Examples\Example2_DataSources.xml"));
            StreamReader sr3 = File.OpenText(Path.Combine(HttpRuntime.AppDomainAppPath, @"App_Data\Examples\Example3_AjaxControls.xml"));
            StreamReader sr4 = File.OpenText(Path.Combine(HttpRuntime.AppDomainAppPath, @"App_Data\Examples\Example4_about.xml"));
            
            PresentationExample1 = sr1.ReadToEnd();
            PresentationExample2 = sr2.ReadToEnd();
            PresentationExample3 = sr3.ReadToEnd();            
            PresentationExample4 = sr4.ReadToEnd();
            

            // type list for screen generation

            ControlTypes objTypes = (ControlTypes)ObjectSerialization.GetObjectFromXmlFile(typeof(ControlTypes), Path.Combine(HttpRuntime.AppDomainAppPath, @"App_Data\TypeList.xml"));

            TypesContainer = ObjectFactory.GetObjectTypes(objTypes.Controls as List<XmlObject>);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            
        }

        
        protected void Application_Error(object sender, EventArgs e)
        {
            
        }

        protected void Session_End(object sender, EventArgs e)
        {
            
        }

        protected void Application_End(object sender, EventArgs e)
        {
            
        }

        
    }
}