﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ScreenGenerationSamplesApp.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<style type="text/css">
    .seadragon
    {
        width: 470px;
        height: 400px;
        background-color: black;
        border: 1px solid black;
        color: white; /* text color for messages */
    }
    .overlay
    {
        background-color: white;
        opacity: 0.7;
        filter: alpha(opacity=70);
        border: 1px solid red;
    }
</style>
<body>
    <form id="form1" runat="server">
    <hr />
    <h1>
        Dynamic Screen Generation</h1>
    <h4>
        Screens are generating based on xml view</h4>
    <h4>
        All controls (html, user, custom, ajax controls etc.) described in xml files
    </h4>
    <hr />
    <asp:ToolkitScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />


    <asp:Panel ID="Panel_buttons" runat="server">
        <asp:LinkButton ID="LinkButton_Standart" runat="server" OnClick="LinkButton_Standart_Click">Standart ASP Controls</asp:LinkButton>
        &nbsp;|
        <asp:LinkButton ID="LinkButton_Data" runat="server" OnClick="LinkButton_Data_Click">Controls with Data Sources</asp:LinkButton>
        &nbsp;|
        <asp:LinkButton ID="LinkButton_ajax" runat="server" OnClick="LinkButton_ajax_Click">Ajax Controls</asp:LinkButton>
        &nbsp;|
        <asp:LinkButton ID="LinkButton_about" runat="server" OnClick="LinkButton_about_Click">About</asp:LinkButton>
    </asp:Panel>
    <hr />
    <asp:Panel ID="Panel_Main" runat="server">
    </asp:Panel>
    </form>    
</body>
</html>

