﻿using System;
using System.Web.UI;
using ACS.Common.IO;
using Presentation.ControlBuilder;
using Presentation.Web.UI;
using WebTest;

namespace ScreenGenerationSamplesApp
{
    public enum screentype { Standart, DataSource, Ajax, About }

    public partial class Default : System.Web.UI.Page
    {
        protected screentype ScreenType
        {
            get { return (screentype)ViewState["screentype"]; }
            set { ViewState["screentype"] = value; } 
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            // generate screen by request 
            if (IsPostBack)
            {
                switch (ScreenType)
                {
                    // only for event binding
                    case screentype.Ajax:
                        LinkButton_ajax_Click(null, null);
                        break;
                }
            }
            else
            {
                LinkButton_Standart_Click(null, null);
            }
            
        }

        protected void LinkButton_Standart_Click(object sender, EventArgs e)
        {
            ScreenType = screentype.Standart;
            GetScreenByXml(Global.PresentationExample1);
        }

        protected void LinkButton_Data_Click(object sender, EventArgs e)
        {
            ScreenType = screentype.DataSource;
            GetScreenByXml(Global.PresentationExample2);
        }

        protected void LinkButton_ajax_Click(object sender, EventArgs e)
        {
            ScreenType = screentype.Ajax;
            GetScreenByXml(Global.PresentationExample3);
        }

        protected void LinkButton_about_Click(object sender, EventArgs e)
        {
            ScreenType = screentype.About;
            GetScreenByXml(Global.PresentationExample4);
        }

        void GetScreenByXml(string xml)
        {
            // add control to page which generated based xml form
            //--------------------------------------------------------------------------------------------------------------------
            Panel_Main.Controls.Clear();
            //Get xml object from file
            XmlObject xmlObject = (XmlObject)StreamAdapter.String2ToXML(typeof(XmlObject), xml);

            //dynamicaly generating screen based on xml
            Control ctrl = (Control)ObjectFactory.Create(Global.TypesContainer, xmlObject.GetFirtsChild(), new CustomObjectAdapter());

            Panel_Main.Controls.Add(ctrl);
            Panel_Main.DataBind();

        }
    }
}