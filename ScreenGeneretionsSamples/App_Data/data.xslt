﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="/">
      <Details>
        
            <xsl:apply-templates select="//Details"/>
      </Details>
        
    </xsl:template>
  <xsl:template match="//Details">
    <Details>
      <xsl:attribute name="Id">
        <xsl:value-of select="Id"/>
      </xsl:attribute>
      <xsl:attribute name="FirstName">
        <xsl:value-of select="FirstName"/>
      </xsl:attribute>
      <xsl:attribute name="LastName">
        <xsl:value-of select="LastName"/>
      </xsl:attribute>
      <xsl:attribute name="Location">
        <xsl:value-of select="Location"/>
      </xsl:attribute>
    </Details>
  </xsl:template>
</xsl:stylesheet>
