﻿using System;

namespace Presentation.Web.UI
{
    public static class ControlExtension
    {

        public static System.Web.UI.Control FindChild(this System.Web.UI.Control ctrl, string id)
        {

            if (ctrl == null)
                throw new ArgumentNullException();

            if(ctrl.ID == id) return ctrl;

            for (int i = 0; i < ctrl.Controls.Count; i++)
            {
                System.Web.UI.Control childCtrl = ctrl.Controls[i].FindChild(id);
                if (childCtrl != null)
                    return childCtrl;
            }

            return null;

        }


        public static System.Web.UI.Control FindParent(this System.Web.UI.Control ctrl, string id)
        {

            if (ctrl == null)
                throw new ArgumentNullException();

            if (ctrl.ID == id) return ctrl;

            return FindParent(ctrl.Parent, id);

        }
    }
}
