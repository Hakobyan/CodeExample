﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using ACS.Common.Collections;


namespace Presentation.Web.UI
{
//    public interface IParameters
//    {
//        Parameters Parameters { get; set; }
//    }

    [XmlRoot]
    public class Handlers
    {
        public Handlers()
        {
            DelegateList = new List<ControlEventHandler>();
        }
        [XmlElement("ControlEventHandler")]
        public List<ControlEventHandler> DelegateList { get; set; }
    }

    [XmlRoot]
    public class ControlEventHandler : IParameters
    {

        public ControlEventHandler()
        {
            Parameters = new Parameters();
        }

        [XmlAttribute]
        public string HandlerModulName { get; set; }

        [XmlAttribute]
        public string HandlerType { get; set; }

        [XmlAttribute]
        public string MethodName { get; set; }

        [XmlAttribute]
        public string HandlerName { get; set; }
                
        [XmlElement]
        public Parameters Parameters { get; set; }
    }
   
}
