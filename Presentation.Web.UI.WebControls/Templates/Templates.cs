﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Xml.Serialization;
using System.Web.UI.WebControls;
using System.Reflection;
using ACS.Common;
using ACS.Common.Collections;
using ACS.Common.Logging;

namespace Presentation.Web.UI
{

    [XmlRoot]
    public class ContentTemplate : System.Web.UI.Control
    {

    }

    [XmlRoot]
    public class Columns : System.Web.UI.Control
    {

    }
     
    [XmlRoot]
    public class BaseTemplate : Control, ITemplate
    {
        public ControlCollection TempControls { get; set; }
                
        public virtual BaseTemplate Clone()
        {
            
            BaseTemplate it = this.ReflectClone() as BaseTemplate;

            foreach (Control c in this.Controls)
            {   
                it.Controls.Add(c);
            }

            return it;
        }

        public BaseTemplate()
        {
            TempControls = new ControlCollection(this);
        }

        public virtual void InstantiateIn(Control container)
        {   
            foreach (Control c in TempControls)
            {   
                container.Controls.Add(c);
            }
        }

    }


  

    [XmlRoot]
    public class ACSItemBoundTemplate : BaseTemplate
    {

        public ACSItemBoundTemplate() : base()
        {
            
        }

        public override void InstantiateIn(Control container)
        {
            foreach (Control c in TempControls)
            {
                Control copyControl = c.Clone() as Control;
                if (c is IParameters)
                    (copyControl as IParameters).Parameters = new Parameters((c as IParameters).Parameters);
                copyControl.DataBinding += new EventHandler(copyControl_DataBinding);
                container.Controls.Add(copyControl);
            }

        }

        public void copyControl_DataBinding(object sender, EventArgs e)
        {
            try
            {
                IDataItemContainer DataItemContainer = (IDataItemContainer)(sender as Control).NamingContainer;


                Parameters dataBindParameters = new Parameters();


                List<string> keys = sender.GetParameterKeysFromProperties(ACS.Common.Collections.Parameter.FIGUR_SCOPE_BEGIN, ACS.Common.Collections.Parameter.FIGUR_SCOPE_END);

                foreach (string key in keys)
                {
                    string dataValue = DataBinder.Eval(DataItemContainer.DataItem, key).ToString();                    
                    dataBindParameters[key] = dataValue.ToString();
                }

                if (dataBindParameters.Count > 0)
                {
                    sender.SetParametersToProperties(dataBindParameters, ACS.Common.Collections.Parameter.FIGUR_SCOPE_BEGIN, ACS.Common.Collections.Parameter.FIGUR_SCOPE_END);
                }

                if (sender is IParameters)
                {
                    if ((sender as IParameters).Parameters != null)
                    {
                        Parameters tempParameters = new Parameters((sender as IParameters).Parameters);
                        foreach (string key in tempParameters.Keys)
                        {
                            Parameters p = new Parameters(DataItemContainer.DataItem is IExchangeParameters ? (DataItemContainer.DataItem as IExchangeParameters).ToParameters() : new Parameters());
                            (sender as IParameters).Parameters[key] = ParametersHelperExtension.Format(tempParameters[key], new Parameters(p, dataBindParameters), ACS.Common.Collections.Parameter.FIGUR_SCOPE_BEGIN, ACS.Common.Collections.Parameter.FIGUR_SCOPE_END);
                        }

                    }
                }

            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw exp;
            }

        }
    }
       
    [XmlRoot]
    public class ItemTemplate : BaseTemplate
    {
 
    }

    [XmlRoot]
    public class HeaderTemplate : BaseTemplate
    {

    }

    [XmlRoot]
    public class EditItemTemplate : BaseTemplate
    {

    }

    [XmlRoot]
    public class FooterTemplate : BaseTemplate
    {

    }

    [XmlRoot]
    public class HeaderStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class FooterStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class RowStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class SelectedRowStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class PagerStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class AlternatingRowStyle : TableItemStyle
    {
 
    }

    [XmlRoot]
    public class ItemStyle : TableItemStyle
    {
 
    }
}
