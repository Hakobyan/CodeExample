﻿using System;
using System.Reflection;
using ACS.Common.Logging;
using System.Web.UI;
using System.Web.UI.WebControls;
using ACS.Common;

namespace Presentation.Web.UI
{
    /// <summary>
    /// adapter pattern for ASP.net standart controls 
    /// </summary>
    public class WebObjectAdapter : ObjectBaseAdapter
    {
        public override void Adapt(Object parentObj, Object childObj)
        {
            try
            {
                base.Adapt(parentObj, childObj);

                if (parentObj.GetType().GetProperty("Controls") != null && (childObj is System.Web.UI.Control))
                {
                    if (parentObj is System.Web.UI.UpdatePanel)
                    {
                        (parentObj as System.Web.UI.UpdatePanel).ContentTemplateContainer.Controls.Add(childObj as System.Web.UI.Control);
                    }
                    else if (parentObj is BaseTemplate)
                    {
                        (parentObj as BaseTemplate).TempControls.Add(childObj as System.Web.UI.Control);
                        //(parentObj as BaseTemplate).Controls.Add(childObj as System.Web.UI.Control);
                    }
                    else
                    {
                        (parentObj as System.Web.UI.Control).Controls.Add(childObj as System.Web.UI.Control);
                    }
                }

                if (parentObj.GetType().GetProperty("Columns") != null && (parentObj is System.Web.UI.WebControls.GridView))
                {
                    if (childObj is System.Web.UI.WebControls.DataControlField || childObj is System.Web.UI.WebControls.TemplateField)
                        (parentObj as System.Web.UI.WebControls.GridView).Columns.Add(childObj as System.Web.UI.WebControls.DataControlField);

                }

                if (parentObj is TemplateField)
                {
                    if (childObj is ItemTemplate)
                        (parentObj as TemplateField).ItemTemplate = (childObj as ItemTemplate);

                    if (childObj is ItemStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as TemplateField).ItemStyle);                        
                    }
                                        
                    if (childObj is HeaderTemplate)
                        (parentObj as TemplateField).HeaderTemplate = (childObj as HeaderTemplate);

                    if (childObj is HeaderStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as TemplateField).HeaderStyle);                        
                    }

                    if (childObj is EditItemTemplate)
                        (parentObj as TemplateField).EditItemTemplate = (childObj as EditItemTemplate);

                    if (childObj is FooterTemplate)
                        (parentObj as TemplateField).FooterTemplate = (childObj as FooterTemplate);

                    if (childObj is FooterStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as TemplateField).FooterStyle);
                    }

                }


                if (parentObj is System.Web.UI.WebControls.GridView)
                {

                    if (childObj is HeaderStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).HeaderStyle);
                    }

                    if (childObj is FooterStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).FooterStyle);
                    }

                    if (childObj is RowStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).RowStyle);                        
                    }

                    if (childObj is SelectedRowStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).SelectedRowStyle);
                    }

                    if (childObj is PagerStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).PagerStyle);
                    }

                    if (childObj is AlternatingRowStyle)
                    {
                        CommonHelper.ReflectCopy(childObj, (parentObj as System.Web.UI.WebControls.GridView).AlternatingRowStyle);
                    }
                }
    

                if (parentObj.GetType().GetProperty("Items") != null && (parentObj is System.Web.UI.WebControls.ListControl))
                {
                    if (childObj is System.Web.UI.WebControls.ListItem)
                        (parentObj as System.Web.UI.WebControls.ListControl).Items.Add(childObj as System.Web.UI.WebControls.ListItem);
                }
              
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }


        }
    }
}

