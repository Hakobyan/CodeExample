﻿using System;
using System.Reflection;
using ACS.Common.Collections;
using ACS.Common.Logging;





namespace Presentation.Web.UI
{
    /// <summary>
    /// adapter pattern for custom realizations
    /// </summary>
    public class CustomObjectAdapter : WebObjectAdapter
    {
        public override void Adapt(Object parentObj, Object childObj)
        {
            try
            {
                base.Adapt(parentObj, childObj);
               

                if (childObj is Handlers)
                {
                    foreach (ControlEventHandler ceh in (childObj as Handlers).DelegateList)
                    {
                        if (String.IsNullOrEmpty(ceh.MethodName))
                            throw new ArgumentNullException(String.Format("MethodName is missing in {0} control handler", (parentObj as System.Web.UI.Control).ID));

                        if (String.IsNullOrEmpty(ceh.HandlerName))
                            throw new ArgumentNullException(String.Format("HandlerName is missing in {0} control handler", (parentObj as System.Web.UI.Control).ID));

                        if (String.IsNullOrEmpty(ceh.HandlerModulName))
                            throw new ArgumentNullException(String.Format("HandlerModulName is missing in {0} control handler", (parentObj as System.Web.UI.Control).ID));

                        if (String.IsNullOrEmpty(ceh.HandlerType))
                            throw new ArgumentNullException(String.Format("HandlerType is missing in {0} control handler", (parentObj as System.Web.UI.Control).ID));
                        
                        Assembly a = Assembly.Load(ceh.HandlerModulName);

                        Type t = a.GetType(ceh.HandlerType);

                        MethodInfo methodInfo = t.GetMethod(ceh.MethodName, BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
                        if (methodInfo != null)
                        {
                            EventInfo eventInfo = parentObj.GetType().GetEvent(ceh.HandlerName);

                            Delegate handler = Delegate.CreateDelegate(eventInfo.EventHandlerType, ceh, methodInfo, false);

                            if (handler != null)
                            {
                                eventInfo.AddEventHandler(parentObj, handler);
                                if ( parentObj is IParameters)
                                {
                                    if ((parentObj as IParameters).Parameters == null)
                                        (parentObj as IParameters).Parameters = new Parameters();

                                    (parentObj as IParameters).Parameters.Add(ceh.Parameters);                                
                                }
                            }
                        }
                    }
                }
                if (parentObj is System.Web.UI.WebControls.TemplateField)
                {
                    if (childObj is ACSItemBoundTemplate)
                        (parentObj as System.Web.UI.WebControls.TemplateField).ItemTemplate = (childObj as ACSItemBoundTemplate);                    
                }


                if (childObj is ControlEventHandler)
                {
                    Handlers hnd = parentObj as Handlers; 
                    ControlEventHandler dg = childObj as ControlEventHandler;
                    hnd.DelegateList.Add(dg);
                }

                if (childObj is Parameters)
                {
                    IParameters ceh = parentObj as IParameters;
                    Parameters p = childObj as Parameters;
                    if (ceh != null)
                    {
                        if (ceh.Parameters == null)
                            ceh.Parameters = new Parameters();
                        ceh.Parameters.Add(p);
                    }
                }

                if (childObj is Parameter)
                {   
                    Parameters prs = parentObj as Parameters;
                    Parameter p = childObj as Parameter;
                    prs[p.Key] = p.Value;
                }
               
            }
            catch (Exception exp)
            {
                LogWrapper.LogError(MethodBase.GetCurrentMethod(), exp);
                throw;
            }


        }
    }
}

