﻿using System;
using Presentation.ControlBuilder;

namespace Presentation.Web.UI
{
    public class ObjectBaseAdapter : IObjectAdapter
    {
        public virtual void Adapt(Object parentObj, Object childObj)
        {

        }
    }
}
